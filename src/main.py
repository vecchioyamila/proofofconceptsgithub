#! python
from google.cloud import bigquery
import google.auth

credentials, project_id = google.auth.default()

client = bigquery.Client("neon-rex-240510")

query_job = client.query("""INSERT INTO ProofofConcepts.t_insertedDates (python_inserted_date) VALUES (CURRENT_DATETIME())""")

results = query_job.result()  # Waits for job to complete.

for row in results:
    print("{} : {} views".format(row.url, row.view_count))